package com.edso.deadletter;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Receiver {

    @Autowired
    RabbitTemplate rabbitTemplate ;


    @RabbitListener(queues = "spring-boot")
    public void receiveMessage(Message message) throws InvalidException {
        System.out.println("Received <" + message + ">");
        throw new InvalidException();
    }

    @RabbitListener(queues = "retry-queue")
    public void retryMessage(Message message) {
        System.out.println("Retry <" + message + ">");
        rabbitTemplate.send(Config.topicExchangeName, message.getMessageProperties()
                .getReceivedRoutingKey(), message);

    }


}