package com.edso.deadletter;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeadLetterApplication {


    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DeadLetterApplication.class, args);
    }

}
