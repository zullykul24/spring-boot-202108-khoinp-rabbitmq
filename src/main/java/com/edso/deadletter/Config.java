package com.edso.deadletter;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class Config {
    static final String topicExchangeName = "spring-boot-exchange";

    static final String queueName = "spring-boot";

    static final String retryExchangeName = "retry-exchange";

    static final String retryQueueName = "retry-queue";

    @Bean
    Queue queue() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", retryExchangeName);
        arguments.put("x-dead-letter-routing-key", "foo.retry.#");
        arguments.put("x-message-ttl", 300);
        arguments.put("x-max-length", 2);
        return new Queue(queueName, false, false, false, arguments);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with("foo.bar.#");
    }

    /////retry
    @Bean
    Queue retryQueue() {

        return new Queue(retryQueueName, false);
    }

    @Bean
    TopicExchange retryExchange() {
        return new TopicExchange(retryExchangeName);
    }

    @Bean
    Binding retryBinding() {
        return BindingBuilder.bind(retryQueue()).to(retryExchange()).with("foo.retry.#");
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

}
